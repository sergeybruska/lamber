$('.mobile-filter-btn').on('click', function (e) {
    e.preventDefault()
    $('.mobile-filter').toggleClass('active')
    $('.mobile-filter-btn').toggleClass('active')
});

var slider = tns({
    container: '.my-slider',
    items: 1,
    nav: false,
    mouseDrag: true,
    touch: true,
    controlsContainer: document.querySelector('.slider-controls'),
    responsive: {
      640: {
        edgePadding: 20,
        gutter: 20,
        items: 1
      },
      700: {
        gutter: 30,
        items:2
      },
      900: {
        items: 3
      }
    }
});

var recipeSlider = tns({
    container: '.recipe-slider',
    items: 1,
    nav: true,
    mouseDrag: true,
    touch: true,
    navContainer: '#customize-thumbnails',
    navAsThumbnails: true,
    controlsContainer: document.querySelector('.recipe-slider-controls'),
    responsive: {
      640: {
        edgePadding: 0,
        gutter: 0,
        items: 1
      },
      700: {
        gutter: 0,
        items:1
      },
      900: {
        items: 1
      }
    }
});

var sliderRange = document.getElementById("myRange");
var output = document.getElementById("rangeValue");
output.innerHTML = sliderRange.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
sliderRange.oninput = function() {
  output.innerHTML = this.value;
}